import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Gene {
    private String name;
    private String function;
    private int beginningBP; // beginning base pair in a chromosome
    private int endingBP; // ending base pair in a chromosome
    private Sequence sequence;

    public Gene(String name, String function, int beginningBP, int endingBP, Sequence sequence) {
        this.name = name;
        this.function = function;
        this.beginningBP = beginningBP;
        this.endingBP = endingBP;
        this.sequence = sequence;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public int getBeginningBP() {
        return beginningBP;
    }

    public void setBeginningBP(int beginningBP) {
        this.beginningBP = beginningBP;
    }

    public int getEndingBP() {
        return endingBP;
    }

    public void setEndingBP(int endingBP) {
        this.endingBP = endingBP;
    }

    public Sequence getSequence() {
        return sequence;
    }

    public void setSequence(Sequence sequence) {
        this.sequence = sequence;
    }

    private List<RS> generateMeasurements(String url){
        List<RS> snps = new ArrayList<>();
        try{
            URL obj = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
            connection.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                Map weather = gson.fromJson(inputLine, Map.class);

                ArrayList<Map> results = (ArrayList<Map>) weather.get("rsid");

                for(Map result : results) {
                    String location = (String) result.get("location");
                    String parameter = (String) result.get("parameter");
                    Map dates = (Map) result.get("date");
                    String dateString = ((String) dates.get("local")).substring(0, 10);
                    int[] dateValues = Arrays.stream(dateString.split("-"))
                            .map(Integer::parseInt)
                            .mapToInt(x -> x)
                            .toArray();

                    LocalDate date = LocalDate.of(dateValues[0], dateValues[1], dateValues[2]);
                    String country = (String)result.get("country");
                    Double value = (Double)result.get("value");
                    /*
                    Location loc = new Location(location, country, city);
                    Measurement measurement = new Measurement(date, loc, parameter, value);
                    measures.add(measurement);

                     */
                }
            }
            in.close();
        }catch(MalformedURLException ex){
            System.out.println("bad url");
        } catch(IOException ex){
            System.out.println("Connection failed");
        }
        return new ArrayList<>();
    }
}
