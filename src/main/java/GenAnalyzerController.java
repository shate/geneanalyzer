import com.mashape.unirest.http.exceptions.UnirestException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class GenAnalyzerController {

    @FXML private Button loadSequenceButton;
    @FXML private TextField beginningBpTextField;
    @FXML private ChoiceBox<Chromosome> chromosomeChoiceBox;
    @FXML private TextArea userSequenceTextField;
    @FXML private Button analyzeButton;
    @FXML private Button previousRsButton;
    @FXML private Button nextRsButton;
    @FXML private Canvas canvas;
    @FXML private Label geneLabel;
    @FXML private Label idLabel;
    @FXML private Label locationLabel;
    @FXML private Label replacementLabel;
    @FXML private ListView<String> diseasesListView;
    @FXML private ListView<String> publicationsListView;
    @FXML private Label rsCountLabel;
    private Analyzer analyzer;
    private Chromosome chromosome;
    private User user;
    Sequence realSequence;
    Sequence userSequence;
    List<HashMap<String, Object>> allThreats;
    ObservableList<String> publications;
    ObservableList<String> diseases;
    private int rsCount = 0;
    GraphicsContext gc;
    double width;
    double height;
    List<SNP> snpList;

    public void initialize() throws IOException, UnirestException {

        File file1 = new File("src/main/resources/blk.txt");

        BufferedReader br1 = new BufferedReader(new FileReader(file1));

        String st1;
        StringBuilder blk = new StringBuilder();
        while ((st1 = br1.readLine()) != null)
            blk.append(st1);

        realSequence = new Sequence(false, String.valueOf(blk), 11450000);
        List<Gene> chromosome8Genes = new ArrayList<>();
        Gene blkGene = new Gene("blk", "function", 11493991, 11564599, realSequence);
        chromosome8Genes.add(blkGene);


        snpList = Analyzer.getApiInfo();
        ObservableList<Chromosome> chromosomesList = FXCollections.observableArrayList(new Chromosome(8, 145138636, "Autosome", chromosome8Genes, snpList));
        chromosomeChoiceBox.getItems().setAll(chromosomesList);
        chromosomeChoiceBox.getSelectionModel().select(0);

        gc = canvas.getGraphicsContext2D();
        width = canvas.getWidth();
        height = canvas.getHeight();

        chromosome = chromosomeChoiceBox.getValue();

    }


    @FXML void analyzeButtonPressed(ActionEvent event) {
        allThreats = analyzer.analyze();
        System.out.println(allThreats.size() + "threats");
        updateRS();
    }

    @FXML
    void loadSequenceButtonPressed(ActionEvent event) throws IOException, UnirestException {
        userSequence = new Sequence(true, null, 11450000);
        System.out.println(userSequence.getSequence().length());

        userSequence.changeNucleotide(11534141, 'G');
        userSequence.changeNucleotide(11486464, 'T');
        userSequence.changeNucleotide(11491677, 'A');
        userSequence.changeNucleotide(11561320, 'C');

        userSequenceTextField.setText(userSequence.getSequence());
        user = new User("Tomek", "Hawro", 22, "Male", 90, 186, userSequence);
        analyzer = new Analyzer(chromosome, user);
    }

    @FXML
    void nextRsButtonPressed(ActionEvent event) {
        if(rsCount + 1 < allThreats.size()) {
            rsCount++;
            updateRS();
        }
    }

    @FXML
    void previousRsButtonPressed(ActionEvent event) {
        if(rsCount - 1 >= 0) {
            rsCount--;
            updateRS();
        }
    }

    private void updateRS(){
        List<String> diseasesList = new ArrayList<>();
        Map<String, Integer> countedDiseases = (Map<String, Integer>)allThreats.get(rsCount).get("diseases");
        for(String pub : countedDiseases.keySet()){
            diseasesList.add(pub + " (" + countedDiseases.get(pub) + ")");
        }

        List<String> pubs = Arrays.asList((String[])allThreats.get(rsCount).get("publications"));
        publications = FXCollections.observableArrayList(pubs);
        publicationsListView.setItems(publications);

        diseases = FXCollections.observableArrayList(diseasesList);
        diseasesListView.setItems(diseases);

        List<String> description= (List<String>)allThreats.get(rsCount).get("description");

        idLabel.setText(description.get(0));
        geneLabel.setText(description.get(1));
        locationLabel.setText(description.get(2));
        replacementLabel.setText(description.get(3));


        rsCountLabel.setText(String.format("%d of %d", rsCount + 1, allThreats.size()));

        draw();
    }

    private void draw(){
        gc.clearRect(0, 0, gc.getCanvas().getWidth(), gc.getCanvas().getHeight());
        List<String> sequenceToDraw = (List<String>)allThreats.get(rsCount).get("nearby");

        System.out.println(sequenceToDraw);
        int location = Integer.parseInt(((List<String>)allThreats.get(rsCount).get("location")).get(0));
        System.out.println(location);
        // Set line width
        gc.setLineWidth(3.0);
        // Set fill color
        gc.setStroke(Color.BLACK);
        Font nucleotideFont = new Font("Arial", 30);
        Font locationFont = new Font("Arial", 10);
        gc.setFont(nucleotideFont);


        double letterSize = (width - 30) / 20;

        for(int i = 0 ; i < sequenceToDraw.size() ; i++){
            if(i == 10){
                gc.setStroke(Color.RED);
                gc.setLineWidth(3.0);
                gc.strokeText(sequenceToDraw.get(i), 15 + letterSize * i, height / 2);
                gc.setLineWidth(3.0);
                gc.setStroke(Color.BLACK);
            }else{
                gc.strokeText(sequenceToDraw.get(i), 15 + letterSize * i, height / 2);
            }
            if((location - 10 + i) % 10 == 0) {
                gc.setFont(locationFont);
                gc.setLineWidth(1.0);
                gc.strokeText(String.format("%d", (location - 10 + i)), 15 + letterSize * i, height / 2 + 50);
                gc.setFont(nucleotideFont);
                gc.setLineWidth(3.0);
            }
        }
    }
}
