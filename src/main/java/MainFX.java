import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Objects;


/**
 * Launches the application
 */
public class MainFX extends Application {

    /**
     * Opens first window
     * @param primaryStage Stage of first window
     * @throws Exception Exception probably caused by not finding a file
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("GenAnalyzer.fxml")));
        primaryStage.setTitle("Blood Bank");
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    /**
     * Launches the application
     * @param args Args entered in terminal
     */
    public static void main(String[] args) {
        launch(args);
    }
}
