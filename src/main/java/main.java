import com.mashape.unirest.http.exceptions.UnirestException;
import java.io.*;
import java.util.*;

public class main {
    public static void main(String[] args) throws IOException, UnirestException {
        /*
        File file1 = new File("src/main/resources/blk.txt");

        BufferedReader br1 = new BufferedReader(new FileReader(file1));

        String st1;
        StringBuilder blk = new StringBuilder();
        while ((st1 = br1.readLine()) != null)
            blk.append(st1);

        File file2 = new File("src/main/resources/blkWycinek.txt");

        BufferedReader br2 = new BufferedReader(new FileReader(file2));

        String st2;
        StringBuilder blkWycinek = new StringBuilder();
        while ((st2 = br2.readLine()) != null)
            blkWycinek.append(st2);



        List<String> publications = new ArrayList<>();
        publications.add("https://www.ncbi.nlm.nih.gov/snp/rs2248932");


        List<String> clinicalSignificance = new ArrayList<>();
        clinicalSignificance.add("SLE");

        List<RS> rsList = new ArrayList<>();
        RS rs2248932 = new RS("rs2248932", 0.49, 11534141, new String[]{"A", "G"}, publications, clinicalSignificance);
        RS rs2736340 = new RS("rs2736340", 0.38, 11486464, new String[]{"C", "T"}, publications, clinicalSignificance);
        RS rs13277113 = new RS("rs13277113", 0.36, 11491677, new String[]{"G", "A"}, publications, clinicalSignificance);
        RS rs758750492 = new RS("rs758750492", 0.0, 11561320, new String[]{"T", "C"}, publications, clinicalSignificance);

        rsList.add(rs2248932);
        rsList.add(rs2736340);
        rsList.add(rs13277113);
        rsList.add(rs758750492);


        Sequence sequence1 = new Sequence(true, null, 0);
        Sequence sequence2 = new Sequence(true, null, 0);

        String function = "functionality";
        Gene blkGene = new Gene("BLK", function, 11450000 , 11570000, sequence1);


        List<Gene> genes = new ArrayList<>();
        genes.add(blkGene);


        Chromosome chromosome8 = new Chromosome(8, 145138636, "Autosome", genes, );

        User tomek = new User("Tomasz", "Hawro", 22,
                "Male", 90, 186, sequence2);

        Analyzer analyzer = new Analyzer(chromosome8, tomek);
        //analyzer.analyze();



        //System.out.println(array.get(0));

        //System.out.println(prettyJsonString);
        //HashMap<String, HashMap<String, Object>> analyzation = analyzer.getApiInfo();

         */
    }


    public static double[][] similarityMatrix(String seq2, String seq1, double mismatch, double match, double gap) {

        int m = seq1.length();
        int n = seq2.length();

        double[][] V = new double[m + 1][n + 1];

        for(int i = 0 ; i <= m ; i++){
            V[i][0] = i * gap;
        }
        for(int j = 0 ; j <= n ; j++){
            V[0][j] = j * gap;
        }
        double cost;
        double a, b, c;
        for(int i = 1 ; i <= m ; i++){
            for(int j = 1 ; j <= n ; j++){
                if(seq1.charAt(i - 1) == seq2.charAt(j - 1)){
                    cost = match;
                }else{
                    cost = mismatch;
                }
                a = V[i - 1][j] + gap;
                b = V[i][j - 1] + gap;
                c = V[i - 1][j - 1] + cost;


                double temp[] = {a, b, c, 0};
                double max = max(temp);

                V[i][j] = max;
            }
        }
        return V;
    }

    public static List<Object> findSimilarityPath(String seq1, String seq2, double[][] V, double mismatch, double match, double gap){
        List<Object> returns = new ArrayList<>();

        double[][] path = new double[V.length][V[0].length];
        StringBuilder aln1 = new StringBuilder();
        StringBuilder aln2 = new StringBuilder();
        double similarity = 0;

        int rows = V.length;
        int columns = V[0].length;

        double max = V[0][0];
        int[] index = {1, 1};

        for(int i = 0 ; i < rows ; i++){
            for(int j = 0 ; j < columns ; j++){
                if(V[i][j] >= max){
                    max = V[i][j];
                    index[0] = i;
                    index[1] = j;
                }
            }
            similarity = max;
        }

        double currentValue = max;
        int i = index[0];
        int j = index[1];

        while(currentValue > 0){
            double cost;
            if((seq1.charAt(j - 1) == seq2.charAt(i - 1)))
                cost = match;
            else
                cost = mismatch;

            if(i > 0 && j > 0 && (V[i][j] == V[i - 1][j - 1] + cost )){
                aln1.insert(0, seq1.charAt(j - 1));
                aln2.insert(0, seq2.charAt(i - 1));

                path[i][j] = 1;
                currentValue = V[i - 1][j - 1];
                i--;
                j--;
            }else{
                if(j > 0 && (V[i][j] == V[i - 1][j] + gap)){
                    aln1.insert(0, "-");
                    aln2.insert(0, seq2.charAt(i - 1));
                    path[i][j] = 1;
                    currentValue = V[i - 1][j];
                    i--;
                }else{
                    aln1.insert(0, seq1.charAt(j - 1));
                    aln2.insert(0, "-");
                    path[i][j] = 1;
                    currentValue = V[i][j - 1];
                    j--;
                }
            }
        }

        StringBuilder connectors = new StringBuilder();
        int identity = 0;
        int gaps = 0;

        for(int n = 0 ; n < aln1.length(); n++){
            if(aln1.charAt(n) == aln2.charAt(n)){
                connectors.append("|");
                identity++;
            }else if(aln1.charAt(n) == '-' || aln2.charAt(n) == '-'){
                gaps++;
                connectors.append(" ");
            }else{
                connectors.append(" ");
            }
        }
        returns.add(path);
        returns.add(aln2.toString());
        returns.add(aln1.toString());
        returns.add(similarity);
        returns.add(connectors);
        returns.add(identity);
        returns.add(gaps);
        return returns;
    }

    public static double min(double[] array){
        double min  = array[0];
        for(double value: array){
            if(value < min){
                min = value;
            }
        }
        return min;
    }

    public static double max(double[] array){
        double max  = array[0];
        for(double value: array){
            if(value > max){
                max = value;
            }
        }
        return max;
    }

    public static void printMatrix(double[][] matrix){
        for(double[] row : matrix){
            for(double value : row){
                System.out.print(value + "   ");
            }
            System.out.println();
        }
    }


}


