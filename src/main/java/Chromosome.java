import java.util.List;

public class Chromosome {
    private int number;
    private int length;
    private String type;
    private List<Gene> genes;
    private List<SNP> snpList;

    public Chromosome(int number, int length, String type, List<Gene> genes, List<SNP> snpList) {
        this.number = number;
        this.length = length;
        this.type = type;
        this.genes = genes;
        this.snpList = snpList;
    }

    public List<SNP> getSnpList() {
        return snpList;
    }

    public void setSnpList(List<SNP> snpList) {
        this.snpList = snpList;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Gene> getGenes() {
        return genes;
    }

    public void setGenes(List<Gene> genes) {
        this.genes = genes;
    }

    @Override
    public String toString() {
        return "Chromosome " + number;
    }
}
