import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import java.util.List;

public class Sequence {
    private List<String> aminoacids;
    private int beginningBP;
    private int endingBP;
    private int[] riskyPlaces;
    private String sequence;

    public Sequence(boolean load, String seq, int beginningBP) throws IOException {
        if(load) {
            generateSequence();
        }else{
            sequence = seq;
        }
        this.beginningBP = beginningBP;
        this.endingBP = beginningBP + sequence.length();
    }

    public int getBeginningBP() {
        return beginningBP;
    }

    public void setBeginningBP(int beginningBP) {
        this.beginningBP = beginningBP;
    }

    public int getEndingBP() {
        return endingBP;
    }

    public void setEndingBP(int endingBP) {
        this.endingBP = endingBP;
    }

    public List<String> getAminoacids() {
        return aminoacids;
    }

    public void setAminoacids(List<String> aminoacids) {
        this.aminoacids = aminoacids;
    }

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public int[] getRiskyPlaces() {
        return riskyPlaces;
    }

    public void setRiskyPlaces(int[] riskyPlaces) {
        this.riskyPlaces = riskyPlaces;
    }

    public void generateSequence() {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File("E:\\Nauka Inżynieria Biomedyczna\\Semestr VI\\TOM"));
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "txt", "txt");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(null);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
            System.out.println("You chose to open this file: " +
                    chooser.getSelectedFile().getName());
        }
        File file1 = chooser.getSelectedFile();

        BufferedReader br1 = null;
        try {
            br1 = new BufferedReader(new FileReader(file1));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        String st1 = null;
        StringBuilder blk = new StringBuilder();
        while (true) {
            try {
                if (!((st1 = br1.readLine()) != null)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            blk.append(st1);
        }

        sequence = String.valueOf(blk);
    }

    public void changeNucleotide(int index, char newNucleotide){
        char[] seqArray = sequence.toCharArray();
        seqArray[index - beginningBP] = newNucleotide;
        sequence =  String.valueOf(seqArray);
    }
}
