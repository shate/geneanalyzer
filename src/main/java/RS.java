import java.util.List;

public class RS {
    private String id;
    private double MAF;
    private int location;
    private String[] replacement;
    private List<String> publications;
    private List<String> clinicalSignificance;

    public RS(String id, double MAF, int location, String[] replacement, List<String> publications, List<String> clinicalSignificance) {
        this.id = id;
        this.MAF = MAF;
        this.location = location;
        this.replacement = replacement;
        this.publications = publications;
        this.clinicalSignificance = clinicalSignificance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getClinicalSignificance() {
        return clinicalSignificance;
    }

    public void setClinicalSignificance(List<String> clinicalSignificance) {
        this.clinicalSignificance = clinicalSignificance;
    }

    public String[] getReplacement() {
        return replacement;
    }

    public void setReplacement(String[] replacement) {
        this.replacement = replacement;
    }

    public double getMAF() {
        return MAF;
    }

    public void setMAF(double MAF) {
        this.MAF = MAF;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public List<String> getPublications() {
        return publications;
    }

    public void setPublications(List<String> publications) {
        this.publications = publications;
    }
}
