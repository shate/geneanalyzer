import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SNP {
    private String id;
    private int chromosome;
    private int location;
    private String geneName;
    private Map<String, Integer> diseases;
    private String[] publications;
    private char[] replacement;
    /*


     */

    public SNP(String id, int chromosome, int location, String geneName, Map<String, Integer> diseases, String[] publications, char[] replacement) {
        this.id = id;
        this.chromosome = chromosome;
        this.location = location;
        this.geneName = geneName;
        this.diseases = diseases;
        this.publications = publications;
        this.replacement = replacement;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getChromosome() {
        return chromosome;
    }

    public void setChromosome(int chromosome) {
        this.chromosome = chromosome;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public String getGeneName() {
        return geneName;
    }

    public void setGeneName(String geneName) {
        this.geneName = geneName;
    }

    public Map<String, Integer> getDiseases() {
        return diseases;
    }

    public void setDiseases(Map<String, Integer> diseases) {
        this.diseases = diseases;
    }

    public String[] getPublications() {
        return publications;
    }

    public void setPublications(String[] pmids) {
        this.publications = pmids;
    }

    public char[] getReplacement() {
        return replacement;
    }

    public void setReplacement(char[] replacement) {
        this.replacement = replacement;
    }

    @Override
    public String toString() {
        return "SNP{" +
                "id='" + id + '\'' +
                ", chromosome=" + chromosome +
                ", location=" + location +
                ", geneName='" + geneName + '\'' +
                ", diseases=" + diseases +
                ", publications=" + Arrays.toString(publications) +
                ", replacement=" + Arrays.toString(replacement) +
                '}';
    }
}
