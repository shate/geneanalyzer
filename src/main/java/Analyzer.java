import com.google.gson.*;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import java.util.*;

public class Analyzer {
    private Chromosome chromosome;
    private User user;

    public Analyzer(Chromosome chromosome, User user) {
        this.chromosome = chromosome;
        this.user = user;
    }

    public Chromosome getChromosome() {
        return chromosome;
    }

    public void setChromosome(Chromosome chromosome) {
        this.chromosome = chromosome;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<HashMap<String, Object>> analyze(){
        List<HashMap<String, Object>> allThreats = new ArrayList<>();
        HashMap<String, Object> singleThreat;
        Map<String, Integer> diseases;
        String[] publications;
        String userSeq = user.getSequence().getSequence();

        char[] userCharSeq = userSeq.toCharArray();

        int riskyIndex;
        for(int i = 0 ; i < userCharSeq.length ; i++) {
            riskyIndex = i + user.getSequence().getBeginningBP();
            singleThreat = new HashMap<>();
            for (SNP snp : chromosome.getSnpList()) {
                if (snp.getLocation() == riskyIndex && userCharSeq[i] == snp.getReplacement()[1]) {
                    diseases = snp.getDiseases();
                    publications = snp.getPublications();
                    singleThreat.put("diseases", diseases);
                    singleThreat.put("publications", publications);
                    List<String> loc = new ArrayList<>();
                    loc.add(String.valueOf(snp.getLocation()));
                    singleThreat.put("location", loc);
                    List<String> nearbySequence = new ArrayList<>();
                    for(int index = 0 ; index < 20 ; index++){
                        nearbySequence.add(String.valueOf(userCharSeq[i - 10 + index]));
                    }
                    singleThreat.put("nearby", nearbySequence);

                    List<String> description = new ArrayList<>();
                    description.add(snp.getId());
                    description.add(snp.getGeneName());
                    description.add(String.valueOf(snp.getLocation()));
                    description.add(String.format("%s > %s", snp.getReplacement()[0], snp.getReplacement()[1]));
                    singleThreat.put("description", description);

                    allThreats.add(singleThreat);
                }
            }
        }
        return allThreats;
    }

    public static List<SNP> getApiInfo() throws UnirestException {
        HashMap<String, HashMap<String, Object>> allRS = new HashMap<>();

        List<SNP> snpList = new ArrayList<>();
        HttpResponse<String> response = Unirest.get("https://www.ncbi.nlm.nih.gov/research/bionlp/litvar/api/v1/entity/search/blk").asString();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(response.getBody());
        JsonArray array = gson.fromJson(je, JsonArray.class);
        HashMap<String, Object> oneRS;
        for(JsonElement rsValue : array){
            oneRS = new HashMap<>();

            Map singleRS = gson.fromJson(rsValue, Map.class);
            String id = (String) singleRS.get("rsid");


            Map diseases = (Map) singleRS.get("diseases");


            Map data = (Map) singleRS.get("data");
            String chromosomeLocation = (String)data.get("chromosome_base_position");
            int chromosome = Integer.parseInt(chromosomeLocation.split(":")[0]);
            int location = Integer.parseInt(chromosomeLocation.split(":")[1]);

            ArrayList genesArray = (ArrayList) data.get("genes");
            Map geneInfo = (Map) genesArray.get(0);
            String geneName = (String)geneInfo.get("name");

            HttpResponse<String> responseHTML = Unirest.get("https://www.ncbi.nlm.nih.gov/snp/" + id + "#clinical_significance").asString();

            String htmlBody = responseHTML.getBody();
            StringTokenizer htmlTokens = new StringTokenizer(htmlBody);
            char[] replacement = new char[2];
            while(htmlTokens.hasMoreTokens()){
                String token = htmlTokens.nextToken();
                if(token.equals("<dt>Alleles</dt>")){
                    htmlTokens.nextToken();
                    String replace = htmlTokens.nextToken();
                    System.out.println(replace);
                    if (replace.contains("del")){
                        replacement[0] = replace.toCharArray()[3];
                        replacement[1] = '-';
                    }else {
                        replacement[0] = replace.toCharArray()[0];
                        replacement[1] = replace.toCharArray()[2];
                    }
                    break;
                }
            }

            oneRS.put("id", id);
            oneRS.put("geneName", geneName);
            oneRS.put("location", location);
            oneRS.put("chromosome", chromosome);
            oneRS.put("replacement", replacement);
            oneRS.put("diseases", diseases);

            allRS.put(id, oneRS);
        }


        // DODAWANIE Z łapy
        oneRS = new HashMap<>();
        oneRS.put("id", "rs2736340");
        oneRS.put("geneName", "BLK");
        oneRS.put("location", 11486464 );
        oneRS.put("chromosome", 8);
        oneRS.put("replacement", new char[]{'C', 'T'});
        Map<String, Integer> dis = new HashMap<>();

        dis.put("Lupus Erythematosus, Systemic", 38);
        dis.put("Infantile polyarteritis", 44);
        dis.put("Arthritis, Rheumatoid", 41);
        dis.put("Autoimmune Diseases", 38);
        dis.put("Mucocutaneous Lymph Node Syndrome", 5);
        oneRS.put("diseases", dis);

        allRS.put("rs2736340", oneRS);


        oneRS = new HashMap<>();
        oneRS.put("id", "rs13277113");
        oneRS.put("geneName", "BLK");
        oneRS.put("location", 11491677  );
        oneRS.put("chromosome", 8);
        oneRS.put("replacement", new char[]{'G', 'A'});
        dis = new HashMap<>();

        dis.put("Lupus Erythematosus, Systemic", 83);
        dis.put("Arthritis, Rheumatoid", 45);
        dis.put("Autoimmune Diseases", 26);
        dis.put("Dermatomyositis", 8);
        dis.put("Polymyositis", 7);
        oneRS.put("diseases", dis);

        allRS.put("rs13277113", oneRS);


        StringBuilder pmidsQuery = new StringBuilder("https://www.ncbi.nlm.nih.gov/research/bionlp/litvar/api/v1/public/rsids2pmids?rsids=");
        int count = 0;
        for(String rsID : allRS.keySet()){
            count++;
            if(count > 1) {
                pmidsQuery.append("%2C").append(rsID);
            }else{
                pmidsQuery.append(rsID);
            }
        }

        HttpResponse<String> responsePMIDS = Unirest.get(String.valueOf(pmidsQuery)).asString();

        Gson gsonPMIDS = new GsonBuilder().setPrettyPrinting().create();

        JsonParser jpPMIDS = new JsonParser();
        JsonElement jePMIDS = jpPMIDS.parse(responsePMIDS.getBody());
        JsonArray arrayPMIDS = gsonPMIDS.fromJson(jePMIDS, JsonArray.class);

        for(JsonElement pmid : arrayPMIDS){

            Map singleRS = gsonPMIDS.fromJson(pmid, Map.class);

            String rsid = (String) singleRS.get("rsid");


            for(String keyRsID : allRS.keySet()){
                if(rsid.equals(keyRsID)){
                    ArrayList pmidsArray = (ArrayList) singleRS.get("pmids");
                    Integer[] pmids = new Integer[pmidsArray.size()];
                    for(int i = 0 ;i < pmidsArray.size() ; i++){
                        pmids[i] = Integer.parseInt(String.format("%.0f", pmidsArray.get(i)));
                    }
                    allRS.get(rsid).put("pmids", pmids);
                }
            }
        }

        for(String rsID : allRS.keySet()){
            HashMap<String, Object> singleRS = allRS.get(rsID);

            String id = rsID;
            int chromosome = (int) singleRS.get("chromosome");
            int location = (int) singleRS.get("location");
            String geneName = (String)singleRS.get("geneName");
            Map<String, Integer> diseases = (Map<String, Integer>)singleRS.get("diseases");
            Integer[] pmids = (Integer[]) singleRS.get("pmids");
            char[] replacement = (char[]) singleRS.get("replacement");
            String[] publications = new String[pmids.length];
            for(int i = 0 ; i < publications.length ; i++){
                publications[i] = String.format("https://www.ncbi.nlm.nih.gov/pubmed/?term=%d", pmids[i]);
            }

            SNP snp = new SNP(rsID, chromosome, location, geneName, diseases, publications, replacement);
            snpList.add(snp);

        }
        return snpList;
    }
}
